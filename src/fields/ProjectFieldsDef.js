import store from '../store'

export default [
    {
        name: 'kode_project',
        title: 'Kode Proyek',
        sortField :'kode_project'
    },
    {
        name: 'nama',
        title: 'Nama Proyek',
        sortField :'nama'
    },    
    {
        name:'Aksi',
        title:'Aksi',
        visible:(store.state.user.role<3)?true:false
    }
]