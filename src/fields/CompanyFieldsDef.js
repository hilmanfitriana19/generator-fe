export default [
    {
        name: 'nama',
        title: 'Nama Perusahaan',
        sortField :'nama'
    },
    {
        name: 'kode_perusahaan',
        title: 'Kode Perusahaan',
        sortField :'kode_perusahaan'
    },    
    {
        name: 'alamat',
        title: 'Alamat',
        formatter(value){
            if (value==null||value=="")return '-';
            return(value.length<100)?value:value.substring(0,100)+'....';

        },
        dataClass:'text-justify',
        width:'20%'
    },
    {
        name: 'no_telp',
        title: 'No Telpon',
        formatter(value){
            return (value==null||value=="")?"-":value;
        }
    },    
    'Aksi'
]
