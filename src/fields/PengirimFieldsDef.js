import store from '../store'

export default [
    {
        name: 'kode_pengirim',
        title: 'Kode Perusahaan',
        sortField :'kode_pengirim'
    },
    {
        name: 'nama',
        title: 'Nama Perusahaan',
        sortField :'nama'
    },  
    {
        name:'Aksi',
        title:'Aksi',
        visible:(store.state.user.role<3)?true:false
    }  
]