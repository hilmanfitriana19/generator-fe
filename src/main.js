import 'mutationobserver-shim'
import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import App from './App.vue'
import router from './router'
import store from './store'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEye,faEyeSlash,faPlusCircle,faTimes, faPencilAlt, faSearch, faTrashAlt, faTools, faSpinner } from '@fortawesome/free-solid-svg-icons'
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import Vuetable from 'vuetable-2';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import loading from 'vuejs-loading-screen'

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)

library.add(faEye,faEyeSlash,faPlusCircle, faTimes,faPencilAlt, faSearch, faTrashAlt, faTools,faSpinner)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component("vuetable", Vuetable);

Vue.use(VueSweetalert2);
Vue.use(loading,{
  bg: '#669999ad',  
  slot: 
  `        
    <h3 class="text-3xl text-white"> Loading...</h3>
  `
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

