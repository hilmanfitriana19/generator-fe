import axios from 'axios';
import store from '../store';

//let base_URL = 'http://localhost:5000';
// let base_URL = 'https://sandbox.vulcanus.space/api';
let base_URL = 'https://number-generator-test.herokuapp.com';
// let base_URL = 'https://number.vulcanus.space/api';

export class apiService {

    login(userData){        
        const url = `${base_URL}/auth/`;        
        return axios.post(url,userData).then(response => response);
    }

    logout(){        
        const url = `${base_URL}/auth/logout`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.post(url,null,{ headers: headers }).then(response => response);
    }

    createUser(data){
        const url = `${base_URL}/user/`;      
        let headers = {"jwt_token": store.state.jwt};  
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }

    getAllUser(){
        const url = `${base_URL}/user/`;      
        let headers = {"jwt_token": store.state.jwt};  
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }


    getUser(username){
        const url = `${base_URL}/user/${username}`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    editUser(username,data) {
        const url = `${base_URL}/user/${username}`;    
        let headers = {"jwt_token": store.state.jwt};    
        return axios.put(url,data,{ headers: headers }).then(response => response);
    }

    deleteUser(username) {
        const url = `${base_URL}/user/${username}`;    
        let headers = {"jwt_token": store.state.jwt};    
        return axios.delete(url,{ headers: headers });
    }

    createRole(data){
        const url = `${base_URL}/role/`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }

    getAllRole(){
        const url = `${base_URL}/role/`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    editRole(id,data) {
        const url = `${base_URL}/role/${id}`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.put(url,data,{ headers: headers }).then(response => response.data);
    }

    deleteRole(id) {
        const url = `${base_URL}/role/${id}`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.delete(url,{ headers: headers });
    }

    createCompany(data){
        const url = `${base_URL}/company/`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }

    getAllCompany(){
        const url = `${base_URL}/company/`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    getCompany(id) {
        const url = `${base_URL}/company/${id}`;     
        let headers = {"jwt_token": store.state.jwt};   
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    editCompany(id,data) {
        const url = `${base_URL}/company/${id}`;     
        let headers = {"jwt_token": store.state.jwt};   
        return axios.put(url,data,{ headers: headers }).then(response => response.data);
    }

    deleteCompany(id) {
        const url = `${base_URL}/company/${id}`;     
        let headers = {"jwt_token": store.state.jwt};   
        return axios.delete(url,{ headers: headers });
    }

    createRecipient(data){
        const url = `${base_URL}/recipient/`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }

    getAllRecipient(){
        const url = `${base_URL}/recipient/`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    getAllRecipientWithoutCompany(){
        const url = `${base_URL}/recipient/non_company`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    getRecipient(id){
        const url = `${base_URL}/recipient/${id}`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    editRecipient(id,data) {
        const url = `${base_URL}/recipient/${id}`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.put(url,data,{ headers: headers }).then(response => response.data);
    }

    deleteRecipient(id) {
        const url = `${base_URL}/recipient/${id}`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.delete(url,{ headers: headers });
    }

    createDocumentNumber(data){
        const url = `${base_URL}/letter/`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }

    createDocumentRevision(id,data){
        const url = `${base_URL}/letter/revision/${id}`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }

    getAllDocumentNumber(){
        const url = `${base_URL}/letter/`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    editDocumentNumber(id,data) {
        const url = `${base_URL}/letter/${id}`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.put(url,data,{ headers: headers }).then(response => response.data);
    }

    deleteDocumentNumber(id) {
        const url = `${base_URL}/letter/${id}`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.delete(url,{ headers: headers });
    }

    getAllFilterBySender(pengirim_id){
        const url = `${base_URL}/letter/pengirim/${pengirim_id}`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    getCompanyRecipient(){
        const url = `${base_URL}/company/recipient`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);        
    }    

    getFile(){
        const url = `${base_URL}/db/`;
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response);
    }
    
    createProject(data){
        const url = `${base_URL}/project/`;      
        let headers = {"jwt_token": store.state.jwt};  
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }

    getAllProject(){
        const url = `${base_URL}/project/`;      
        let headers = {"jwt_token": store.state.jwt};  
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    getProject(id){
        const url = `${base_URL}/project/${id}`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    editProject(id,data) {
        const url = `${base_URL}/project/${id}`;    
        let headers = {"jwt_token": store.state.jwt};    
        return axios.put(url,data,{ headers: headers }).then(response => response);
    }

    deleteProject(id) {
        const url = `${base_URL}/project/${id}`;    
        let headers = {"jwt_token": store.state.jwt};    
        return axios.delete(url,{ headers: headers });
    }

    createKodeSurat(data){
        const url = `${base_URL}/kodeSurat/`;      
        let headers = {"jwt_token": store.state.jwt};  
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }

    getAllKodeSurat(){
        const url = `${base_URL}/kodeSurat/`;      
        let headers = {"jwt_token": store.state.jwt};  
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    getKodeSurat(kode){
        const url = `${base_URL}/kodeSurat/${kode}`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    editKodeSurat(kode,data) {
        const url = `${base_URL}/kodeSurat/${kode}`;    
        let headers = {"jwt_token": store.state.jwt};    
        return axios.put(url,data,{ headers: headers }).then(response => response);
    }

    deleteKodeSurat(kode) {
        const url = `${base_URL}/kodeSurat/${kode}`;    
        let headers = {"jwt_token": store.state.jwt};    
        return axios.delete(url,{ headers: headers });
    }

    getAllPengirim(){
        const url = `${base_URL}/pengirim/`;      
        let headers = {"jwt_token": store.state.jwt};  
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    getPengirim(kode){
        const url = `${base_URL}/pengirim/${kode}`;        
        let headers = {"jwt_token": store.state.jwt};
        return axios.get(url,{ headers: headers }).then(response => response.data);
    }

    editPengirim(kode,data) {
        const url = `${base_URL}/pengirim/${kode}`;    
        let headers = {"jwt_token": store.state.jwt};    
        return axios.put(url,data,{ headers: headers }).then(response => response);
    }

    deletePengirim(kode) {
        const url = `${base_URL}/pengirim/${kode}`;    
        let headers = {"jwt_token": store.state.jwt};    
        return axios.delete(url,{ headers: headers });
    }

    createPengirim(data){
        const url = `${base_URL}/pengirim/`;      
        let headers = {"jwt_token": store.state.jwt};  
        return axios.post(url,data,{ headers: headers }).then(response => response);
    }
}

export const apiServices = new apiService();
