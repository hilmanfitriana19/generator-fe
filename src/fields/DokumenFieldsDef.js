export default [
    {
        name: 'no_surat',
        title: 'No Dokumen',
        sortField :'no_surat'        
    },
    {
        name: 'subject',
        title: 'Subject',
        sortField :'subject',
    },
    {
        name: 'kode_surat',
        title: 'Kode Dokumen',
        sortField :'kode_surat',
        
    },
    {
        name: 'kode_project',
        title: 'Proyek',
        sortField :'kode_project'
    },
    {
        name: 'tujuan',
        title: 'Tujuan',
        sortField :'tujuan'
    },
    {
        name: 'penerima',
        title: 'Penerima',
        sortField :'penerima'
    },
    {
        name: 'person_in_charge',
        title: 'PIC',
        sortField :'person_in_charge'
    },    
    {
        name: 'alias',
        title: 'Alias',
        formatter(value){
            return (value==null||value=="")?"-":value;
        }
    },        
    'Aksi'
]
