import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import {isValidJwt} from '../utils'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    jwt:null,
    user:{
      name:null,      
      role:null,
      code_name:null
    },
  },
  mutations: {    
    resetState(state) {      
      state.jwt=null;
      state.user.name=null;      
      state.user.role=null;
      state.user.code_name=null;
      localStorage.jwt=null;      
    },
    setData(state, data) {        
      state.jwt=data.jwt_token;
      state.user.name=data.user;
      state.user.role=data.role;
      state.user.code_name=data.code_name;
      localStorage.jwt=data.jwt_token;
    },    
  },  
  getters:{
    isLoggedIn:state=>state.jwt!=null,
    isAuthenticated(state) {
      return function (now) {
        return isValidJwt(state.jwt, now)
      };  
    },
  
  },
  plugins: [
    createPersistedState()
  ]
})