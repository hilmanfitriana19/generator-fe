import store from '../store'

export default [
    {
        name: 'id_penerima',
        title: 'ID Penerima',
        sortField:'id_penerima'
    },
    {
        name: 'nama',
        title: 'Nama Penerima',
        sortField:'nama'
    },
    {
        name: 'jabatan',
        title: 'Jabatan',
        formatter(value){
            return (value==null||value=="")?"-":value;
        }
    },
    {
        name: 'email',
        title: 'Email',
        formatter(value){
            return (value==null||value=="")?"-":value;
        }
    },
    {
        name: 'no_telp',
        title: 'No Telpon',
        formatter(value){
            return (value==null||value=="")?"-":value;
        }
    },
    {
        name:'Aksi',
        title:'Aksi',
        visible:(store.state.user.role<3)?true:false
    }
]
