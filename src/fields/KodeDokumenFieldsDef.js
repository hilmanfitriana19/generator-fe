import store from '../store'

export default [
    {
        name: 'kode_surat',
        title: 'Kode Dokumen',
        sortField :'kode_surat'
    },
    {
        name: 'nama',
        title: 'Dekripsi',
    },
    {
        name:'Aksi',
        title:'Aksi',
        visible:(store.state.user.role<3)?true:false
    }
]