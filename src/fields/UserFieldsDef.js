
import store from '../store'

export default [
    {
        name: 'name',
        title: 'Nama',
        sortField :'name'
    },
    {
        name:'role',
        title:'Roles',
        visible:(store.state.user.role==1)?true:false
    },
    {
        name: 'username',
        title: 'Username',
        sortField :'username'
    },
    {
        name: 'code_name',
        title: 'Code Name',
        sortField :'code_name'
    },
    
    'Aksi'    
]
