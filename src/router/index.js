import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Home from '../views/Home.vue'
import store from '../store'
import DokumenData from '../component/tables/DokumenTable.vue'
import CompanyData from '../component/tables/CompanyTable.vue'
import RecipientData from '../component/tables/RecipientTable.vue'
import UserData from '../component/tables/UserTable.vue'
import UserForm from '../component/forms/UserForm.vue'
import ProjectData from '../component/tables/ProjectTable.vue'
import PengirimData from '../component/tables/PengirimTable.vue'
import KodeDokumenData from '../component/tables/KodeDokumenTable.vue'
import CompanyForm from '../component/forms/CompanyForm.vue'
import RecipientForm from '../component/forms/RecipientForm.vue'
import ProjectForm from '../component/forms/ProjectForm.vue'
import PengirimForm from '../component/forms/PengirimForm.vue'
import KodeDokumenForm from '../component/forms/KodeDokumenForm.vue'

Vue.use(VueRouter)

export const routes = [
  {
    path: '/auth',
    name: 'login',
    component: Login
  },
  {
    path: '/',
    name: 'home',
    component: Home,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/dokumen',
    name: 'dokumenData',
    component: DokumenData,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/penerima',
    name: 'companyData',
    component: CompanyData,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/company/:id',
    name: 'edit-company',
    component: CompanyForm,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/recipient',
    name: 'recipientData',
    component: RecipientData,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/recipient/:id',
    name: 'edit-recipient',
    component: RecipientForm,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/user',
    name: 'userData',
    component: UserData,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/user/:id',
    name: 'editUser',
    component: UserForm,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/user/create',
    name: 'userForm',
    component: UserForm,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/project',
    name: 'projectData',
    component: ProjectData,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/project/:id',
    name: 'edit-project',
    component: ProjectForm,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/pengirim',
    name: 'pengirimData',
    component: PengirimData,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/pengirim/:id',
    name: 'edit-pengirim',
    component: PengirimForm,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },
  {
    path: '/data/kode_dokumen',
    name: 'kodeDokumenData',
    component: KodeDokumenData,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },  
  {
    path: '/data/kode_dokumen/:id',
    name: 'editKodeDokumen',
    component: KodeDokumenForm,
    beforeEnter (to, from, next) {
      if (!store.getters.isAuthenticated(Date.now())) {    
        next('/auth')
      } else {              
        next()
      }
    },  
  },  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
